import styled from 'styled-components'

export const Container = styled.div `

    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    header {
        width: 70%;
        display: flex;
        flex-direction: row;
        justify-content: start;
        align-items: center;
        height: 100px;

        div {
            display: flex;
            flex-direction: column;
        }

        h1 {
            text-align: left;
            font: Bold 28px/35px Muli;
            letter-spacing: 0;
            color: #3C3C3C;
            opacity: 1;
        }

        h2 {
            text-align: left;
            padding-top: 5px;
            text-align: left;
            font: Bold 14px/18px Muli;
            letter-spacing: 0;
            color: #747474;
            opacity: 1;
        }


        button {
            width: 42px;
            height: 42px;
            border-radius: 18px;
            background: #3b5bfd;
            border: 0;
            cursor: pointer;
        }
    }

    ul {
        margin-top: 30px;
    }
   
`
export const TypeRow = styled.div `
    display: flex;
    flex-direction: row;
    justify-content: center;
    
    height: 100%;
    width: 70%;
`
export const TypeCol = styled.div `
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 14%;
    height: 100%;
    flex: 0 0 300px;
`

export const Logo = styled.div `
    width: 35px;
    height: 35px;
    background: transparent url('../../img/Imagem 1.png') 0% 0% no-repeat padding-box;
    border-radius: 18px;
    opacity: 1;
    margin-right: 30px;
    margin-left: 30px;
`


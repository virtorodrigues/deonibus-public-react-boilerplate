import React from 'react'
import { useSelector } from 'react-redux'
import ListPassage from '../ListPassage'
import Filter from '../Filter'
import Favorit from '../Favorit'
import ActionFavorit from '../CounterFavorit'
import { 
  Container, 
  TypeRow, 
  Logo, 
  TypeCol 
} from './styles'

export default function Board() {
  
    const passages = useSelector(state => state.passageReducer.passages)
    const filterClass = useSelector(state => state.filterReducer.filterClass) 
    const filterLeave = useSelector(state => state.filterReducer.filterLeave) 
    const filterPrice = useSelector(state => state.filterReducer.filterPrice) 
    
    let hoursFilter = ''
    let datePassage = ''
    let hoursPassage = ''
    let minutesPassage = ''
    const usePassages = passages.filter( passage => (
      filterClass.filter( filter => filter == passage.BusClass ).length > 0
      && filterLeave.length > 0 && (filterLeave.filter( filter => {

        hoursFilter = filter.split(":")
        datePassage = new Date(passage.DepartureDate.iso)
        hoursPassage = datePassage.getHours()
        minutesPassage = datePassage.getMinutes()
        if(hoursFilter[0] <= hoursPassage && (parseInt(hoursFilter[0])+5) > hoursPassage) return true
        if((parseInt(hoursFilter[0])+5) == hoursPassage && hoursFilter[1] >= minutesPassage) return true
        return false
      } )).length > 0 
      && filterPrice >= passage.Price )
    )
    
    usePassages.sort(function(a,b) {
      a = new Date(a.DepartureDate.iso)
      b = new Date(b.DepartureDate.iso)
      if(a < b) return -1;
      if(a > b) return 1;
      return 0;
    })

    return (
      <Container>
          <header>
              <Logo />
              <div>
                <h1>Passagem de ônibus de São Paulo para Rio de Janeiro</h1>
                <h2>Data de embarque: 08/09/2019</h2>
              </div>
          </header>
          <TypeRow>            
            { <ListPassage data={ usePassages } /> }

            <TypeCol>
              { <Filter/> }
              { <ActionFavorit /> }
            </TypeCol>

          </TypeRow>
          <Favorit />
      </Container>
    )
}
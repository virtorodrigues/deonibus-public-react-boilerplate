import styled from 'styled-components'

export const Container = styled.div `
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 57%;
    padding-right: 30px;
    flex: 0 0 970px;
`
export const Content = styled.div `
    width: 100%;
    height: 40px;
    height: 40px;
    background: #FFFFFF 0% 0% no-repeat padding-box;
    opacity: 1;

    header {
        width: 70%;
        display: flex;
        flex-direction: row;
        justify-content: start;
        align-items: center;
        height: 100px;

        div {
            display: flex;
            flex-direction: column;
        }

        h2 {
            text-align: left;
            font: Bold 28px/35px Muli;
            letter-spacing: 0;
            color: #3C3C3C;
            opacity: 1;
        }

        p {
            text-align: left;
            padding-top: 5px;
        }


        button {
            width: 42px;
            height: 42px;
            border-radius: 18px;
            background: #3b5bfd;
            border: 0;
            cursor: pointer;
        }
    }
`

export const Body = styled.div `
    height: 34px;
    display: flex;
    flex-direction: row;
    align-items: center;
`

export const LabelHeaderList = styled.span `
    text-align: left;
    font: 400 13px/16px Muli;
    letter-spacing: 0;
    color: #747474;
    opacity: 1;
`

export const DetailHeader = styled.div `
    height: 6px;
    background: #E61D41 0% 0% no-repeat padding-box;
    border-radius: 5px 5px 0px 0px;
    opacity: 1;
`

export const Logo = styled.div `
    width: 35px;
    height: 35px;
    background: transparent url('../../img/Imagem 1.png') 0% 0% no-repeat padding-box;
    border-radius: 18px;
    opacity: 1;
    margin-right: 30px;
    margin-left: 30px;
`


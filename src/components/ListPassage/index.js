import React from 'react'
import { useSelector } from 'react-redux'
import ItemList from '../ItemPassage'

import { 
  Container, 
  Content, 
  DetailHeader, 
  LabelHeaderList, 
  Body 
} from './styles'
import { 
  Label, 
  Image, 
  Leave, 
  Road, 
  BusClass, 
  Price, 
  Duration 
} from '../ItemPassage/styles'

export default function ListPassage({ data }) {
    
    const favoritsQtd = useSelector(state => state.favoritReducer.favoritsQtd) 

    return (
      <Container>
          <Content>
            <DetailHeader />
            <Body>
              <Image />
              <Leave>
                <LabelHeaderList>Saída</LabelHeaderList>
              </Leave>
              <Road>
                <LabelHeaderList>Embarque/Desembarque</LabelHeaderList>
              </Road>
              <Duration>
                <LabelHeaderList>Duração</LabelHeaderList>
              </Duration>
              <BusClass>
                <LabelHeaderList>Saída</LabelHeaderList>
              </BusClass>
              <Price>
                <LabelHeaderList>Preço</LabelHeaderList>
              </Price>
              <Label />
            </Body>
          </Content>
          { data.map((result) => <ItemList key={result.objectId} favoritsQtd={favoritsQtd} data={result}/> ) }
      </Container>
    )
}
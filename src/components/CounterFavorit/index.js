import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { 
    ContainerBody, 
    Container, 
    ContainerListHeader, 
    Checked, 
    DetailHeader, 
    AddToFavorit 
} from './styles'

export default function CounterFavorit() {
    const dispath = useDispatch()
    const favoritsQtd = useSelector(state => state.favoritReducer.favoritsQtd) 
    const passages = useSelector(state => state.passageReducer.passages) 
    const favoritsPassages = useSelector(state => state.favoritReducer.favoritsPassages) 
    const showAddFavorit = useSelector(state => state.favoritReducer.showAddFavorit) 
    const favoritsQtdShowMessage = useSelector(state => state.favoritReducer.favoritsQtdShowMessage) 

    function confirmFavorit() {
        if(favoritsQtd.length > 0) {
            dispath({ type: 'ADD_QTD_FAVORIT_SHOW_MESSAGE', qtd: favoritsQtd.length })
            dispath({ type: 'CHANGE_SHOW_Add_FAVORIT' })
            
            dispath({ type: 'REMOVE_ITEM_PASSAGE', 
                payload: passages.filter((passage) => (
                    passage.objectId != favoritsQtd.filter((favorit)=> passage.objectId == favorit)
                )) 
            })
            dispath({ type: 'CLEAR_FAVORITS' })
    
            dispath({ type: 'ADD_ITEM_FAVORITS',
                payload: favoritsPassages.concat(passages.filter((passage) => (
                    passage.objectId == favoritsQtd.filter((favorit)=> passage.objectId == favorit)
                )))
            })
    
            setTimeout(function(){
                dispath({ type: 'CHANGE_SHOW_Add_FAVORIT' }) 
                 }, 4000);
        }
    }
    return (
        <Container favoritsQtd={favoritsQtd.length}>
            <ContainerListHeader>
              <DetailHeader />
                <ContainerBody>
                    <h1>{favoritsQtd.length}</h1>
                    <p>passagens selecionadas</p>
                    <button onClick={ confirmFavorit }>CONFIRMAR FAVORITOS</button>
                </ContainerBody>
            </ContainerListHeader>
            <AddToFavorit showAddFavorit={showAddFavorit}>
                <title>{favoritsQtdShowMessage} passagens adicionadas como favoritas com sucesso!</title><Checked />
            </AddToFavorit>
        </Container>
    )
}
import styled, { css } from 'styled-components'

export const Container = styled.div `

    height: 12%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
    padding-left: 30px;
    padding-bottom: 30px;

    h1 {
        text-align: left;
        font: 900 27px/34px Muli;
        letter-spacing: 0;
        color: #3C3C3C;
        opacity: 1;
    }

    p {
        text-align: left;
        font: 600 9px/11px Muli;
        letter-spacing: 0;
        color: #3C3C3C;
        opacity: 1;
        padding-bottom: 20px;
    }

    ${props => props.favoritsQtd == 0 && css && 
        `button {
            background: #C7C7C7 0% 0% no-repeat padding-box;
            opacity: 1;
            color: #fff
            display: flex;
            align-items: center;

            padding: 10px;
            border-radius: 5px;

            font: 900 12px/15px Muli;
            letter-spacing: 0.97px;
            color: #8A8A8A;

            border: none;
            transition: .2s ease-in-out;
        
            outline: inherit;

        }` ||
        ` button {
            background: #008760 0% 0% no-repeat padding-box;
            opacity: 1;
            color: #fff
            display: flex;
            align-items: center;

            padding: 10px;
            border-radius: 5px;

            font: 900 12px/15px Muli;
            letter-spacing: 0.97px;
            color: #FFFFFF;
            opacity: 1;

            border: none;
            transition: .2s ease-in-out;
        
            outline: inherit;

            cursor: pointer;
        }

        button:hover {
            opacity: 0.8;
        }
    `}

`
export const ContainerListHeader = styled.div `
    width: 100%;
    height: 100%;
    background: #FFFFFF 0% 0% no-repeat padding-box;
    opacity: 1;
    box-shadow: 0px 0px 8px #00000029;
    border-radius: 5px;
`

export const DetailHeader = styled.div `
    height: 6px;
    background: #E61D41 0% 0% no-repeat padding-box;
    border-radius: 5px 5px 0px 0px;
    opacity: 1;
`

export const ContainerBody = styled.div `
   display: flex;
   align-items: center;
   flex-direction: column;
   padding: 10px;
`

export const AddToFavorit = styled.div `
    width: 344px;
    height: 66px;
    top: 101px;
    left: -0px;
    
    background: #26CB5D 0% 0% no-repeat padding-box;
    box-shadow: 3px 3px 6px #00000029;
    opacity: 1;

    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;

    padding: 25px;

    ${props => props.showAddFavorit ? `
        left: 0;
    ` : `
        left: -500px;
    `}

    transition: left 0.4s;

    position: fixed;
   
    title {
        display: flex;
        text-align: left;
        font: 700 11px/14px Muli;
        letter-spacing: 0;
        color: #FFFFFF;
        opacity: 1;
        cursor: pointer;
        align-items: center;
        padding-right: 40px;
    }
`

export const Checked = styled.div`
    position: relative;
    width: 30px;
    height: 19px;
    background: transparent url('../../img/checkAddFavorit.svg') 0% 0% no-repeat padding-box;
    opacity: 1;
`

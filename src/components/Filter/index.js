import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { 
    TypeRow, 
    Body, 
    ClassBus, 
    Price, 
    Leave, 
    Container, 
    Content, 
    DetailHeader 
} from './styles'

import { getPrice } from '../../utils'

export default function Filter() {
   
    const dispath = useDispatch()
    const filterPrice = useSelector(state => state.filterReducer.filterPrice) 

    function _filterClass(event) {
        const checked = event.target.checked
        const value = event.target.value

        checked ? 
        dispath({ type: 'ADD_FILTER_CLASS', payload: value }) 
        : dispath({ type: 'REMOVE_FILTER_CLASS', payload: value })
    }

    function _filterLeave(event) {
        const checked = event.target.checked
        const value = event.target.value

        checked ? dispath({ type: 'ADD_FILTER_LEAVE', payload: value }) : dispath({ type: 'REMOVE_FILTER_LEAVE', payload: value })
    }

    function getValueRange(event) {
        const value = event.target.value
        dispath({ type: 'CHANGE_FILTER_PRICE', payload: value })
    }

    return (
        <Container>
            <Content>
                <DetailHeader />
                <Body>
                    <TypeRow>
                        <ClassBus>
                            <title>Classe</title>
                            <label>
                                <input value="Convencional" type="checkbox" onChange={e => _filterClass(e)}  />
                                Convencional
                            </label>
                            <label>
                                <input value="Executivo" type="checkbox" onChange={e => _filterClass(e)}  />
                                Executivo
                            </label>
                            <label>
                                <input value="Semi Leito" type="checkbox" onChange={e => _filterClass(e)}  />
                                Semi-leito
                            </label>
                            <label>
                                <input value="Leito" type="checkbox" onChange={e => _filterClass(e)}  />
                                Leito
                            </label>
                        </ClassBus>
                        <Price>
                            <title>Preço máximo</title>
                            <label>
                                {getPrice(filterPrice)}
                            </label>
                            <input onChange={ (e)=> getValueRange(e) } type='range' step={2} min={1} max={6000} value={filterPrice} id="myRange" />
                        </Price>
                    </TypeRow>
                
                    <Leave>
                        <title>Saída</title>
                        
                        <label> 
                        <input value="00:00" type="checkbox" onChange={e => _filterLeave(e)}  />
                            Madrugada - (00h00 - 05h59)
                        </label>
                        
                        <label>
                            <input value="06:00 " type="checkbox" onChange={e => _filterLeave(e)}  />
                            Manhã - (06h00 - 11h59)
                        </label>
                        <label>
                            <input value="12:00" type="checkbox" onChange={e => _filterLeave(e)}  />
                            Tarde - (12h00 - 17h59)
                        </label>
                        <label>
                            <input value="18:00" type="checkbox" onChange={e => _filterLeave(e)}  />
                            Noite - (18h00 - 23h00)
                        </label>
                    </Leave>
                </Body>
            </Content>            
        </Container>
    )
}
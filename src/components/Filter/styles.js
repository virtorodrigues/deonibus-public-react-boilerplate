import styled from 'styled-components'

export const Container = styled.div `

  height: 16%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
  padding-left: 30px;
  padding-bottom: 30px;

  label {
      text-align: left;
      font: 400 11px/14px Muli;
      letter-spacing: 0;
      color: #3C3C3C;
      opacity: 1;
      display: flex;
      align-items: center;
      padding-bottom: 4px;
  }

  input {
      margin-right: 5px;
  }

  title {
      text-align: left;
      font: 700 13px/16px Muli;
      letter-spacing: 0;
      color: #3C3C3C;
      opacity: 1;
      display: block;
      padding-bottom: 5px;
  }

  input[type='range'] {
    width: 100%;
    height: 20px
    overflow: hidden;
    cursor: pointer;
  }
  input[type='range'],
  input[type='range']::-webkit-slider-runnable-track,
  input[type='range']::-webkit-slider-thumb {
    -webkit-appearance: none;
  }
  input[type='range']::-webkit-slider-runnable-track {
    width: 100%;
    height: 2px;
    background: #E61D41 0% 0% no-repeat padding-box;
    border-radius: 285px;
    opacity: 1;
  }
  input[type='range']::-webkit-slider-thumb {
    position: relative;
    margin-top: -5px;
    border-radius: 50%;
    width: 11px;
    height: 11px;
    background: #E61D41 0% 0% no-repeat padding-box;
    opacity: 1;
  }

  input[type="checkbox"] {
    -webkit-appearance: none;
    
    /* Styling checkbox */
    width: 13px;
    height: 13px;
    background: #FFFFFF 0% 0% no-repeat padding-box;
    border: 1px solid #E9E9E9;
    opacity: 1;
    border-radius: 3px;

  }
  
  input[type="checkbox"]:checked {
    background: #008760 0% 0% no-repeat padding-box;
    border: 1px solid #FFFFFF;
    opacity: 1;
  }
 
`
export const Content = styled.div `
    width: 100%;
    height: 100%;
    background: #FFFFFF 0% 0% no-repeat padding-box;
    opacity: 1;
    box-shadow: 0px 0px 8px #00000029;
    border-radius: 5px;
`

export const DetailHeader = styled.div `
    height: 6px;
    background: #E61D41 0% 0% no-repeat padding-box;
    border-radius: 5px 5px 0px 0px;
    opacity: 1;
`

export const TypeRow = styled.div `
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding-bottom: 10px;
`

export const ClassBus = styled.div `
    display: flex;
    flex-direction: column;
`

export const Price = styled.div `
    display: flex;
    flex-direction: column;
`

export const Leave = styled.div `
    display: flex;
    flex-direction: column;
`
export const Body = styled.div `
   padding: 10px;
`
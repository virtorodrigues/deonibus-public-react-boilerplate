import React from 'react'
import { useDispatch } from 'react-redux'

import { 
    Container, 
    DetailPassagePlaceDate, 
    DetailPassagePriceClass, 
    DetailPassageRemove, 
    LabelPrice, 
    LabelBusClass 
} from './styles'

import { getBoarding, getPrice } from '../../utils'


export default function ItemFavorit({ data }) {

    const dispatch = useDispatch()

    function removeFavorit(e) {
        dispatch({ type: 'REMOVE_ITEM_FAVORIT', id: e.objectId })
        dispatch({ type: 'ADD_ITEM_PASSAGE', payload: e })
    }

    return (
        <Container>             
            <DetailPassagePlaceDate>
                <p>Origem: { data.Origin }</p>            
                <p>Destino: { data.Destination }</p>
                <p>Embarque: { getBoarding(data.DepartureDate.iso) }</p>
            </DetailPassagePlaceDate>
            <DetailPassagePriceClass>
                <LabelPrice>{ getPrice(data.Price) }</LabelPrice>        
                <LabelBusClass>{ data.BusClass }</LabelBusClass> 
            </DetailPassagePriceClass>
            <DetailPassageRemove>
                <button onClick={ () => removeFavorit(data) } />
            </DetailPassageRemove>
        </Container>
    )
}


import styled, { keyframes } from 'styled-components'


export const showItem = keyframes `
    0% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
`

export const Container = styled.div `

    height: 63px;
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    
    animation-name: ${showItem};
    animation-duration: 0.5s;
    animation-timing-function: ease;
    animation-delay: 0s;
    animation-iteration-count: 1;
    animation-direction: normal;
    animation-fill-mode: forwards;
    animation-play-state: running;

    margin-bottom: 10px;

    p {
        font: 400 9px/11px Muli;
        letter-spacing: 0;
        color: #FFFFFF;
        opacity: 1;
    }

    button {
        cursor: pointer;
        width: 11px;
        height: 12px;
        background: transparent url('../../img/trash.svg') 0% 0% no-repeat;
        opacity: 1;

        border: none;
      
        outline: inherit;
    }
`

export const DetailPassagePlaceDate = styled.div `

    height: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 70%;
`

export const DetailPassagePriceClass = styled.div `

    height: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 20%;
    align-items: center;
`

export const DetailPassageRemove = styled.div `

    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 10%;
    align-items: flex-end;
`

export const LabelPrice = styled.span `
    font: 700 13px/16px Muli;
    letter-spacing: 0;
    color: #FFFFFF;
    opacity: 1;
`
export const LabelBusClass = styled.span `
    font: 600 8px/10px Muli;
    letter-spacing: 0;
    color: #FFFFFF;
    opacity: 1;
`

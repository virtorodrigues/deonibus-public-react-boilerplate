import styled from 'styled-components'

export const Container = styled.div `

    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 21%;

    padding: 20px;

    background: #3C3C3C 0% 0% no-repeat padding-box;
    box-shadow: -8px 0px 13px #00000029;
    opacity: 1;

    transition: right 0.4s;

    position: fixed;
    right: -30%;
    top: 0;
    
    ${props => props.showFavorit ? `
        right: 0;
    ` : `
        right: -30%;
    `}

    title {
        display: block;
        text-align: left;
        font: Bold 11px/14px Muli;
        letter-spacing: 0;
        color: #FFFFFF;
        opacity: 1;
        cursor: pointer;
        align-self: baseline;
        padding-bottom: 30px;
    }
  

`

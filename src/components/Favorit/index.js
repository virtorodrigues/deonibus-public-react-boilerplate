import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Container } from './styles'
import ItemFavorit from '../ItemFavorit'

export default function Favorit() {
    const favoritsPassages = useSelector(state => state.favoritReducer.favoritsPassages) 
    const showFavorit = useSelector(state => state.favoritReducer.showFavorit)

    const dispatch = useDispatch()

    function changeShowFavorit() {
        dispatch({ type: 'CHANGE_SHOW_FAVORIT' })
    }

    return (
        <Container showFavorit={showFavorit}>             
            <title onClick={changeShowFavorit}>Passagens favoritas   > </title>
            { favoritsPassages.map((result) => <ItemFavorit key={result.objectId} data={ result } /> ) }
        </Container>
    )
}
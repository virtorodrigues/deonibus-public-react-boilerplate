import React from 'react'
import { useDispatch } from 'react-redux'
import { 
    Container, 
    Label, 
    Image, 
    Leave, 
    Road,
    BusClass, 
    Price, 
    Duration, 
    LeaveDeparture, 
    LeaveArrive,
    Checked
} from './styles'

import { getHourFormated, getDuration, getPrice } from '../../utils'

export default function ItemPassage({ data, favoritsQtd }) {    
    const dispath = useDispatch()
    
    function addFavorit(data) {
        dispath({ type: 'ADD_QTD_FAVORIT', id: data.objectId })
    }

    return (
        <Container>
            <Image>
                { data.Company.Name }
            </Image>
            <Leave>
                <LeaveDeparture>{ getHourFormated(data.DepartureDate.iso) }</LeaveDeparture>
                <LeaveArrive>{ getHourFormated(data.ArrivalDate.iso) }</LeaveArrive>
            </Leave>
            <Road>
                <p>{ data.Origin }</p>            
                <p>{ data.Destination }</p>            
            </Road>
            <Duration>
                <p>{ getDuration(data.DepartureDate.iso, data.ArrivalDate.iso) }</p>
            </Duration>
            <BusClass>
                <p>{ data.BusClass }</p>            
            </BusClass>
            <Price>
                <p>{ getPrice(data.Price) }</p>            
            </Price>
            { favoritsQtd.filter((favorit) => favorit === data.objectId ).length > 0 
                && <Label>Escolhido<Checked /></Label> 
                || <button onClick={ ()=> addFavorit(data) }>ESCOLHER</button> }
        </Container>
    )
}
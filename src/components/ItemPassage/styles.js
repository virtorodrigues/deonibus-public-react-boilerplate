import styled, { keyframes } from 'styled-components'

export const showItem = keyframes `
    0% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
`

export const Container = styled.div `

    position: relative;
    background: #fff;
    margin-bottom: 15px;
    background: #FFFFFF 0% 0% no-repeat padding-box;
    box-shadow: 0px 6px 8px #00000029;
    opacity: 0;
    width: 100%;
    height: 121px;
    display: flex;
    align-items: center;
    
    animation-name: ${showItem};
    animation-duration: 0.5s;
    animation-timing-function: ease;
    animation-delay: 0s;
    animation-iteration-count: 1;
    animation-direction: normal;
    animation-fill-mode: both;
    animation-play-state: running;

    div {
        display: flex;
        flex-direction: column;
    }

    img {
        width: 24px;
        height: 24px;
        border-radius: 2px;
        margin-top: 5px;
    }

    button {
        width: 20%;
        background: #008760 0% 0% no-repeat padding-box;
        border-radius: 5px;
        opacity: 1;

        padding: 10px;
        margin-right: 10px;

        font: 900 11px/14px Muli;
        letter-spacing: 0.89px;
        color: #FFFFFF;
        opacity: 1;

        border: none;
        cursor: pointer;
        transition: .2s ease-in-out;
      
        outline: inherit;

        cursor: pointer;
    }

    button:hover {
        opacity: 0.8;
    }
`
export const Image = styled.div`
    text-align: center;
    width: 20%;
`

export const Leave = styled.div`
    text-align: center;
    width: 10%;
`

export const Road = styled.div`
    text-align: left;
    width: 20%;

    p {
        font: 600 15px/19px Muli;
        color: #ACACAC;
        opacity: 1;
    }
`

export const Duration = styled.div`
    text-align: center;
    width: 10%;
    
    p {
        font: 600 15px/19px Muli;
    }
`

export const BusClass = styled.div`
    text-align: center;
    width: 20%;

    p {
        font: 600 15px/19px Muli;
    }
`

export const Price = styled.div`
    text-align: center;
    width: 20%;

    p {
        font: 900 20px/25px Muli;
        color: #3C3C3C;
        opacity: 1;
    }
`

export const LeaveDeparture = styled.p`
    font: 900 17px/21px Muli;
    color: #3C3C3C;
    opacity: 1;
    line-height: 21px;
`
export const LeaveArrive = styled.p`
    font: 600 17px/21px Muli;
    color: #9C9C9C;
    opacity: 1;
    line-height: 21px;
`

export const Label = styled.span`
    text-align: center; 
    width: 20%;
    transition: .2s ease-in-out;
    margin-right: 13px;

    cursor: pointer
    font: 900 12px/15px Muli;
    letter-spacing: 0.1px;
    color: #008760;
    opacity: 0.8;
    display: flex;
    align-items: center;
    justify-content: center;
`
export const Checked = styled.div`
    border: 1px solid transpartent
    width: 20px;
    height: 15px;
    background: transparent url('../../img/check.svg') 0% 0% no-repeat padding-box;
    opacity: 1;
    margin-left: 10px;
`



import React from 'react'
import { ListPassage } from '../../services/api'
import { useDispatch } from 'react-redux'

import { Container, Menu, Favorit } from './styles'

export default function Header() {
    ListPassage()
    const dispatch = useDispatch()

    function changeShowFavorit() {
        dispatch({ type: 'CHANGE_SHOW_FAVORIT' })
      }

    return (
        <Container>
          
            <Menu />
            <button onClick={ changeShowFavorit }><Favorit/>Passagens favoritas</button>

        </Container>
    )
}
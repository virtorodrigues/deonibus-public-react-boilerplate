import styled from 'styled-components'

export const Container = styled.div `

    height: 54px;
    color: #fff;
    position: relative;
    display: flex;
    align-items: center;
    background: #E61D41 0% 0% no-repeat padding-box;
    opacity: 1;

    button {
        right: 14%;
        position: absolute;
        font: 900 11px/14px Muli;
        letter-spacing: 0;
        color: #FFFFFF;
        opacity: 1;

        background: transparent;

        border: none;
        padding: 12px 16px;
        cursor: pointer;

        display: flex;
        align-items: center;
    }
`

export const Menu = styled.div `
    left: 14%;
    position: absolute;
    border: 1px solid transpartent
    width: 20px;
    height: 15px;
    background: transparent url('../../img/menu.svg') 0% 0% no-repeat padding-box;
    opacity: 1;
`

export const Favorit = styled.div `
    border: 1px solid transpartent
    width: 25px;
    height: 14px;
    background: transparent url('../../img/thumbsup.svg') 0% 0% no-repeat padding-box;
    opacity: 1;
`
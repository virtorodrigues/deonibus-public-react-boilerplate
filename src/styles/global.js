import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`

    @import url('https://fonts.googleapis.com/css?family=Muli:400,600,700,900&display=swap');

    * {
        margin: 0;
        padding: 0;
        outline: 0;
        box-sizing: border-box;
    }

    html, body, #root {
        height: 100%;
        width: 100%;
    }

    

    body {
        font-family: 'Muli', sans-serif;
        font-size: 14px;
        background: #DEDEDE;
        color: #333;
        -webkit-font-smoothing: antialiased !important;
    }

    ul {
        list-style: none;
    }
`;
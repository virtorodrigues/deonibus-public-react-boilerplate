import React from 'react';

import Header from '../components/Header'
import Board from '../components/Board'
import GlobalStyle from '../styles/global'

class HomePageContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    
    return (
      <>
        <Header />
        <Board />
        <GlobalStyle />
      </>
    );
  }
}

export default HomePageContainer;
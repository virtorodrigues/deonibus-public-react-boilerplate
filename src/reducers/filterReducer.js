import initialState from './initialState';

export default function filterReducer(state = initialState, action) {
  switch(action.type) {

    case 'ADD_FILTER_CLASS':
      return { ...state, // copy state
        filterClass: [ ...state.filterClass, action.payload ] }

    case 'REMOVE_FILTER_CLASS':
      return { ...state, // copy state
        filterClass: state.filterClass.filter((filter) => filter != action.payload) }
    
    case 'ADD_FILTER_LEAVE':
      return { ...state, // copy state
        filterLeave: [ ...state.filterLeave, action.payload ] }

    case 'REMOVE_FILTER_LEAVE':
      return { ...state, // copy state
        filterLeave: state.filterLeave.filter((filter) => filter != action.payload) }

    case 'CHANGE_FILTER_PRICE':
      return { ...state, // copy state
        filterPrice: action.payload }

    default: return state;
  }
}

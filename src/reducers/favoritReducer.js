import initialState from './initialState';

export default function favoritReducer(state = initialState, action) {
  switch(action.type) {

    case 'ADD_QTD_FAVORIT':
      return { ...state, // copy state
        favoritsQtd: [ ...state.favoritsQtd, action.id ] }

    case 'ADD_QTD_FAVORIT_SHOW_MESSAGE':
      return { ...state, // copy state
        favoritsQtdShowMessage: action.qtd }

    case 'CLEAR_FAVORITS':
      return { ...state, favoritsQtd: [] }
 
    case 'CHANGE_SHOW_FAVORIT':
      return { ...state, // copy state
        showFavorit: !state.showFavorit }

    case 'CHANGE_SHOW_Add_FAVORIT':
      return { ...state, // copy state
        showAddFavorit: !state.showAddFavorit }
            
            
        
    case 'ADD_ITEM_FAVORITS':
      return { ...state, favoritsPassages: action.payload }
        
    case 'REMOVE_ITEM_FAVORIT':
      return { ...state, // copy state
        favoritsPassages: state.favoritsPassages.filter((filter) => filter.objectId != action.id),
        passages: state.passages.concat(state.favoritsPassages.filter((filter) => filter.objectId == action.id))
      }
    
    default: return state;
  }
}

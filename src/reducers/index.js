// Set up your root reducer here...
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import favoritReducer from './favoritReducer';
import filterReducer from './filterReducer';
import passageReducer from './passageReducer';

 const rootReducer = history => combineReducers({
  router: connectRouter(history),
  favoritReducer,
  filterReducer,
  passageReducer
});

 export default rootReducer;
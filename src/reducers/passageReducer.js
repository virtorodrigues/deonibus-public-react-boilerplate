import initialState from './initialState';

export default function passageReducer(state = initialState, action) {
  switch(action.type) {

    case 'GET_PASSAGES':
      return { ...state, passages: action.payload }
    
    case 'ADD_ITEM_PASSAGE':
      return { ...state, // copy state
        passages: [ ...state.passages, action.payload ]
      }
    
    case 'REMOVE_ITEM_PASSAGE':
      return { ...state, // copy state
        passages: action.payload }

    default: return state;
  }
}

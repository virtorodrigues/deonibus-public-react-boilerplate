export function getHourFormated(dt){
    dt = new Date(dt)
    let hours = dt.getHours();
    let minutes = dt.getMinutes();

    return formatZero(hours)+':'+formatZero(minutes)
}

export function hourformatedDuration(hour, minute) {
    return formatZero(hour)+'h '+formatZero(minute)+'m'
}

function formatZero(data){
    if(parseInt(data) < 10)
        return '0'+data
    return data
}

export function getBoarding(dt){
    dt = new Date(dt)

    let day = dt.getDay()
    let month = dt.getMonth()
    let year = dt.getFullYear()

    let hour = dt.getHours()
    let minutes = dt.getMinutes()

    return `${formatZero(day)}/${formatZero(month)}/${year} - ${formatZero(hour)}:${formatZero(minutes)}`
}

export function getDuration(departure, arrival){
    let dtDeparture = new Date(departure).getTime()
    let dtArrival = new Date(arrival).getTime()

    let resultHr = dtArrival - dtDeparture

    let hours = Math.floor((resultHr % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((resultHr % (1000 * 60 * 60)) / (1000 * 60));

    return hourformatedDuration(hours, minutes)
}

export function getPrice(money){
    money = parseFloat(money).toFixed(2).split('.');
    money[0] = "R$ " + money[0].split(/(?=(?:...)*$)/).join('.');
    return money.join(',');
}
